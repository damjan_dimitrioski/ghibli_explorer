# Demo

user: root
pass: D4+mgQ/6pe4^B3..


# Project setup

## GNU/Linux setup
### Base packages
```sudo apt install git curl``` 

### Python packages
```sudo apt install python3-dev virtualenv```

## Project

we use -p if python3 is not default python version

```virtualenv -p /usr/bin/python3 venv```

or 

```virtualenv venv```

otherwise.


```
source venv/bin/activate
pip install -r requirements.txt
```

Apply all the application migration files 
```
python manage.py migrate
```

Load fixtures data

```
python manage.py loaddata  catalog/fixtures/user.json
```

Create the cache

```
python manage.py createcachetable
```


Run the server

```
python manage.py runserver
```


# Movies endpoint
Opening the view for the first time will result in appending cache header keys

[http://127.0.0.1:8000/movies](http://127.0.0.1:8000/movies)

```

Date: Thu, 17 Sep 2018 10:20:17 GMT
Expires: Thu, 17 Sep 2018 10:21:17 GMT
Cache-Control: max-age=60
```
Approximately it will run for 16 - 17 seconds, on the first run.

Any additional refresh will respond with the same content, until 60 seconds pass


# Testing

Load environment first.

```source venv/bin/activate```

Test whether movies route works

```python manage.py test catalog.tests.CatalogMoviesViewTests.test_movies_view_caching movie_catalog```


Test whether caching up to 1 minute works

```python manage.py test catalog.tests.CatalogMoviesViewTests.test_movies_view movie_catalog```

Test all
```python manage.py test catalog.tests.CatalogMoviesViewTests movie_catalog```
