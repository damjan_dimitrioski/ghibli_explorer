from django.shortcuts import render
from . import api_ghibli as api


def movies(request):
    titles = api.fetch_titles()

    return render(template_name='index.html', request=request, context={'films': titles})
