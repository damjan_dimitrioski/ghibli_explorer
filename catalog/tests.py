import time

from django.test import TestCase
from django.conf import settings

API_URL = '/movies'


# region Helpers

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        t = te - ts

        return result, t

    return timed

# endregion


class CatalogMoviesViewTests(TestCase):
    @timeit
    def fetch_titles(self):
        return self.client.get(API_URL)

    def test_movies_view(self):
        response = self.client.get(API_URL)
        self.assertEqual(response.status_code, 200)

    def test_movies_view_caching(self):

        print('initial data')
        data, initial_time = self.fetch_titles()
        print('initial data end: %s' % initial_time)

        i = 0
        global_time = round(initial_time)

        while global_time <= settings.API_TIMEOUT_SECONDS:
            i += 1
            global_time += 10
            time.sleep(10)

            print('iteration #%s' % i)
            current_data, total_time = self.fetch_titles()

            if global_time > settings.API_TIMEOUT_SECONDS:
                self.assertGreaterEqual(initial_time, total_time)

            print('iteration #%s stop, with time: %s.' % (i, total_time))

        print('overall time: %s' % global_time)
