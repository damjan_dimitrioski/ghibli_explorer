import contextlib
import json
import logging
from urllib.request import urlopen
from urllib import error


API_URL = 'https://ghibliapi.herokuapp.com/films'


# region Helpers
def read_url(url):
    """
    :type url: str HTTP URI path to fetch
    """

    # If the status is not 200 will crash
    try:
        # Ensure we close the connection
        with contextlib.closing(urlopen(url)) as req:
            resp = req.read()

            try:
                return json.loads(resp)
            except ValueError:
                logging.exception('JSON parsing url\'s data: %s.' % url)
    except error.HTTPError:
        logging.exception('Reading url: %s failed.' % url)

    # In case of an error we ensure we have empty data
    return []
# endregion


def map_people_list(people):
    """
    Convert list of people endpoint to list of dictionary containing people objects
    :type people: list
    :return list
    """

    return [read_url(p) for p in people if not p.endswith('people/')]


def fetch_titles():
    films_data_json = read_url(API_URL)
    for film in films_data_json:
        film['people'] = map_people_list(film.get('people'))

    return films_data_json
